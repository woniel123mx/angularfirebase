import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore} from '@angular/fire/firestore';

declare var firebase: any;


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: {
    nombre?: string;
    foto?: string;
    correo?: string;
  } = {};
  constructor(
    public firebaseAuth: AngularFireAuth,
    public db:AngularFirestore
  ) {

  }
   

  loginWithGoogle() {
    return this.firebaseAuth.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    ).then((data => {
      this.userData.correo = data.user.email;
      this.userData.foto = data.user.photoURL;
      this.userData.nombre = data.user.displayName;
      return data;
    }))
  }


  logout() {
    return this.firebaseAuth.auth.signOut();
  }
}
