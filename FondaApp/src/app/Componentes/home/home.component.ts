import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/Services/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  nuevoPedidos: {
    nombreComida?: string;
    cantidad?: number;
  }[] = [];
  constructor(public authService: AuthService, public route: Router, private _snackBar: MatSnackBar) { }
  gotoAdministrador() {
    this.route.navigate(['administrador']);
  }
  ngOnInit() {

  }
  AgregarComida(nombreComida: string, cantidad: number) {
    let tmp: {
      nombreComida?: string;
      cantidad?: number;
      fecha?: Date;
      estatus?: string;
    } = {};
    tmp.nombreComida = nombreComida;
    tmp.cantidad = cantidad;
    this.nuevoPedidos.push(tmp);
  }
  SolicitarComida() {
    let toSend = [];
    for (const iterator of this.nuevoPedidos) {
      let tmp: {
        nombreComida?: string;
        cantidad?: number;
        fecha?: Date;
        estatus?: string;
      } = {};
      tmp.nombreComida = iterator.nombreComida;
      tmp.cantidad = iterator.cantidad;
      tmp.fecha = new Date();      
      toSend.push(tmp);
    }
    this.authService.db.firestore.collection("pedidos").add({
      user: this.authService.userData.correo,
      estatus : "NUEVO",
      pedido: toSend
    }).then(docRef => {
      this._snackBar.open("Se mando tu orden correctamente.", "", { duration: 2000 });
    })
      .catch(error => {
        console.error("Error adding document: ", error);
      });

  }

  CerrarSesion() {
    this.authService.logout();
  }
}
