import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService } from 'src/app/Services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, AfterViewInit {
  pedidos: {
    cantidad?: number,
    platillo?: string,
    correo?: string,
    fecha?: Date,
    estatus?:string,
    idPedido?:string,
  }[] = [];
  Eliminar(item:any){
    this.authService.db.firestore.collection("pedidos").doc(item.idPedido).delete();
  }
  Despachar(item:any){
    this.authService.db.firestore.collection("pedidos").doc(item.idPedido).update({
      estatus:"EN RUTA"
    });
  }
  ngAfterViewInit(): void {
    this.authService.db.firestore.collection("pedidos")
      .onSnapshot(querySnapshot => {
        this.pedidos = [];
        querySnapshot.forEach(doc => {
          console.log(doc.id, " => ", doc.data());
          let data = doc.data();
          for (const iterator of data.pedido) {
            let newPedido: any = {};
            newPedido.cantidad = iterator.cantidad;
            newPedido.platillo = iterator.nombreComida;
            newPedido.correo = data.user;
            newPedido.fecha = new Date(iterator.fecha.seconds * 1000);
            newPedido.idPedido = doc.id;
            newPedido.estatus = data.estatus;
            this.pedidos.push(newPedido);
          }
        })
      });
  }
  constructor(public authService: AuthService, public route: Router) {

  }
  getData() {
    return this.pedidos.sort(this.sortFunction);
  }
  sortFunction(a, b) {
    var dateA = new Date(a.date).getTime();
    var dateB = new Date(b.date).getTime();
    return dateA > dateB ? 1 : -1;
  };
  CerrarSesion() {
    this.authService.logout();
  }
  ngOnInit() {
  }

}
