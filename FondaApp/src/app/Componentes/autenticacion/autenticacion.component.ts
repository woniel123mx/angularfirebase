import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/Services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-autenticacion',
  templateUrl: './autenticacion.component.html',
  styleUrls: ['./autenticacion.component.css']
})
export class AutenticacionComponent implements OnInit {

  constructor(private firebaseAuth: AuthService, private router: Router) { }

  ngOnInit() {
  }
  Login() {

    this.firebaseAuth.loginWithGoogle().then((data) => {
      this.router.navigate(['home']);
    });
  }

}
