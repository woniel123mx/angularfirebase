import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutenticacionComponent } from './Componentes/autenticacion/autenticacion.component';
import { AppComponent } from './app.component';

const routes: Routes = [
    { path: "", component: AppComponent},
    { path: "login", component: AutenticacionComponent},
    
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
