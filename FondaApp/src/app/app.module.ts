import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatCardModule, MatToolbar, MatToolbarModule, MatDividerModule, MatListModule, MatSidenavModule, MatAccordion, MatExpansionModule, MatSelectModule, MatInputModule, MatMenuModule, MatSnackBarModule } from '@angular/material';

import { AppComponent } from './app.component';
import { AutenticacionComponent } from './Componentes/autenticacion/autenticacion.component';
import { AngularFireAuth, AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireModule } from "@angular/fire";
import {
  AngularFireDatabaseModule,
  AngularFireDatabase
} from "@angular/fire/database";
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AuthService } from './Services/auth.service';
import { HomeComponent } from './Componentes/home/home.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AdminComponent } from './Componentes/admin/admin.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

export const firebaseConfig = {
  apiKey: "AIzaSyAiNLlkakdCpVij2OqZJ9Vn6tdtUw_pid8",
  authDomain: "fondaapp.firebaseapp.com",
  databaseURL: "https://fondaapp.firebaseio.com",
  projectId: "fondaapp",
  storageBucket: "fondaapp.appspot.com",
  messagingSenderId: "130973502761",
  appId: "1:130973502761:web:c4d607710018b7e5"
};
const routes: Routes = [
  { path: "", component: AppComponent },
  { path: "login", component: AutenticacionComponent },
  { path: "home", component: HomeComponent },
  { path: "administrador", component: AdminComponent },


];

@NgModule({
  declarations: [
    AppComponent,
    AutenticacionComponent,
    HomeComponent,
    AdminComponent,



  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatToolbarModule,
    MatDividerModule,
    MatListModule,
    MatSidenavModule,    
    MatExpansionModule,
    MatSelectModule,
    MatInputModule,
    MatMenuModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    BrowserModule,
    NgbModule,    
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    RouterModule.forRoot(routes),

  ],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
