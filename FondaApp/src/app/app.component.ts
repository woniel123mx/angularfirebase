import { Component } from '@angular/core';
import { AuthService } from './Services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FondaApp';
  constructor(private fireService: AuthService,private router:Router) {
    
    this.fireService.firebaseAuth.authState.subscribe(auth=>{      
      if(auth==null){
        this.router.navigate(['login']);
      }else{
        this.fireService.userData.correo = auth.email;
        this.fireService.userData.foto = auth.photoURL;
        this.fireService.userData.nombre = auth.displayName;
        this.router.navigate(['home']);
      }
    })
  }
}
